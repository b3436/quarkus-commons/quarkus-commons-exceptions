# Quarkus Commons Exceptions

This project was created to centralize components related to exceptions and their related handling among personal backend projects.

# Exception Pattern

The parent exception is based on [RFC-7807](https://datatracker.ietf.org/doc/html/rfc7807)
in order to standardize child exceptions. See below the fields present in the parent exception:

- **Code** is the HTTP _status code_.
- **Title** is a _short description_ about what happened.
- **Description** is a _detailed description_ about what happened.
- **Type** is the _exception name_.

This structure does not need to be followed faithfully by the child exceptions, they can create other fields for your purpose.
