# 📖 Description

In this block, focus in write a **short description** of the **bug or hotfix** and what is happened with the final user.

# 🗺 Step by Step

In this block, focus on writing the **step by step** to arrive at the **occurrence** how you think is better. Try to putting images
for simplify the descriptions.

# ⚒ Implementation

In this block, focus on writing the **resolution** citing created components how you think is better. Try to putting images
for simplify the descriptions.

# 📄 Notes

This block is optional, then if you have some notes to cite put in this block.
