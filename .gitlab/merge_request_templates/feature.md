# 📖 Description

In this block, focus in write the **feature description** and why she is important for the project domain.

# ⚒ Implementation

In this block, focus on writing the **implementation description** citing created components such as:
endpoint classes, port interfaces, domain objects, application use cases, repositories or critical decisions made during development.

# 📄 Notes

This block is optional, then if you have some notes to cite put in this block.
